package mx.tec.jan28;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class MainActivity extends AppCompatActivity {

    // Local storage

    // local database sqlite

    // files
    // prefs

    private DBHelper db;
    private EditText id, name, grade;

    // properties
    // for java applications
    // a way to save key-value set on local storage
    private Properties properties;
    private static final String PROPERTIES_FILE="properties.xml";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DBHelper(this);
        id = findViewById(R.id.editText);
        name = findViewById(R.id.editText2);
        grade = findViewById(R.id.editText3);

        properties = new Properties();

        File file = new File(getFilesDir(), PROPERTIES_FILE);

        if(file.exists()){

            Toast.makeText(this, "LOADING FILE", Toast.LENGTH_SHORT).show();
            try {
                FileInputStream fis = openFileInput(PROPERTIES_FILE);
                properties.loadFromXML(fis);
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (InvalidPropertiesFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "CREATING FILE", Toast.LENGTH_SHORT).show();
            saveProperties();
        }

    }

    private void saveProperties(){
        try {
            FileOutputStream fos = openFileOutput(PROPERTIES_FILE, Context.MODE_PRIVATE);
            properties.storeToXML(fos, null);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveToMemory(View v){
        properties.put("example", "A value to save");
        Toast.makeText(this, "SAVING TO MEMORY...", Toast.LENGTH_SHORT).show();
    }

    public void saveToFile(View v){
        saveProperties();
        Toast.makeText(this, "SAVING TO FILE...", Toast.LENGTH_SHORT).show();
    }

    public void printProperty(View v){
        Toast.makeText(this, "PROPERTY: " + properties.get("example"), Toast.LENGTH_SHORT).show();
    }


    public void save(View v){

        db.save(name.getText().toString(), Integer.parseInt(grade.getText().toString()));
        Toast.makeText(this, "RECORD SAVED", Toast.LENGTH_SHORT).show();
    }

    public void find(View v){

        Log.wtf("TEST", "SO FAR SO GOOD");
        int result = db.find(name.getText().toString());
        Log.wtf("TEST", "SO FAR SO GOOD 2");
        grade.setText(result + "");
        Toast.makeText(this, "FINDING...", Toast.LENGTH_SHORT).show();
    }

    public void delete(View v){

        int rows = db.delete(Integer.parseInt(id.getText().toString()));
        Toast.makeText(this, rows + " rows affected.", Toast.LENGTH_SHORT).show();
    }

    public void changeActivity(View v){

        Intent i = new Intent(this, SharedPrefsActivity.class);
        startActivity(i);
    }
}
